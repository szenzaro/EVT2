import { Component, Input } from '@angular/core';

import { GenericElementData } from '../../services/xml-parsers/generic-parser.service';

@Component({
  selector: 'evt-generic-element',
  templateUrl: './generic-element.component.html',
  styleUrls: ['./generic-element.component.scss'],
})
export class GenericElementComponent {

  @Input() data: GenericElementData;

}
