import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

declare var $: any;

@Component({
  selector: 'evt-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {
  @Input() data: any; // TODO: Create Type

  @ViewChild('noteTrigger') noteTrigger: ElementRef;
  @ViewChild('noteContent') noteContent: ElementRef;

  content: string;

  ngOnInit() {
    const noteContent = $(this.noteContent.nativeElement);
    $(this.noteTrigger.nativeElement).popover({
      trigger: 'focus',
      placement: 'auto',
      html: true,
      content: function () {
        return noteContent.html();
      }
    });
  }

}
