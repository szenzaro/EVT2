import { Component, OnInit, Input } from '@angular/core';

import { GenericParserService } from '../../services/xml-parsers/generic-parser.service';

@Component({
  selector: 'evt-content-viewer',
  templateUrl: './content-viewer.component.html',
  styleUrls: ['./content-viewer.component.scss'],
})
export class ContentViewerComponent implements OnInit {
  @Input() content: HTMLElement;

  parsedContent: any;

  constructor(
    private parser: GenericParserService
  ) {
  }

  ngOnInit() {
    this.parser.parse(this.content).then(parsedContent => this.parsedContent = parsedContent);
  }
}
