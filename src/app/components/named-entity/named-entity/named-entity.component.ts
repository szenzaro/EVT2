import { Component, Input } from '@angular/core';

import { DomUtilsService } from '../../../services/dom-utils.service';

@Component({
  selector: 'evt-named-entity',
  templateUrl: './named-entity.component.html',
  styleUrls: ['./named-entity.component.scss']
})
export class NamedEntityComponent {
  @Input() entityId: string;

  constructor(
    public domUtils: DomUtilsService
  ) {
  }

}
