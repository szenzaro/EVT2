import { Component, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'evt-named-entity-ref',
  templateUrl: './named-entity-ref.component.html',
  styleUrls: ['./named-entity-ref.component.scss']
})
export class NamedEntityRefComponent {
  @Input() data: any; // TODO: Create Type

  private visible = false;

  constructor(
    public elementRef: ElementRef
  ) {
  }

  goToEntityInList() {
    this.visible = !this.visible;
  }
}
