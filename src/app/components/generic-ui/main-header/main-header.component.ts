import { Component, OnInit } from '@angular/core';
import { LocaleService, TranslationService, Language } from 'angular-l10n';

import { ConfigurationService } from '../../../configuration/configuration.service';

import { UiConfig, EditionConfig } from '../../../configuration/evt-config';
@Component({
  selector: 'evt-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss']
})
export class MainHeaderComponent implements OnInit {
  @Language() lang: string;
  uiConfig: UiConfig;
  editionConfig: EditionConfig;

  constructor(
    public locale: LocaleService,
    public translation: TranslationService,
    public configuration: ConfigurationService) { }

  ngOnInit() {
    this.loadUiConfig();
    this.loadEditionConfig();
  }

  private loadUiConfig(): void {
    this.configuration.getUiConfig()
      .subscribe(uiConfig => this.uiConfig = uiConfig);
  }

  private loadEditionConfig(): void {
    this.configuration.getEditionConfig()
      .subscribe(editionConfig => this.editionConfig = editionConfig);
  }

  selectLanguage(language: string): void {
    this.locale.setCurrentLanguage(language);
  }
}
