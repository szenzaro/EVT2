import { Component, Input } from '@angular/core';

import { TextData } from '../../services/xml-parsers/generic-parser.service';

@Component({
  selector: 'evt-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent {

  @Input() data: TextData;

}
