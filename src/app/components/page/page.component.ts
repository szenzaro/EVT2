import { Component, Input } from '@angular/core';

import { PageData } from '../../edition-data/page';

import { DomUtilsService } from '../../services/dom-utils.service';

@Component({
  selector: 'evt-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent {
  @Input() data: PageData;

  constructor(
    public domUtils: DomUtilsService
  ) {
  }

}
