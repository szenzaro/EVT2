import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import { ConfigurationService } from '../../configuration/configuration.service';
import { StructureXmlParserService } from '../../services/xml-parsers/structure-xml-parser.service';
import { DomUtilsService } from '../../services/dom-utils.service';

import { FileConfig } from '../../configuration/evt-config';

@Component({
  selector: 'evt-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss'],
})
export class DocumentComponent implements OnInit {
  fileConfig: FileConfig;

  editionSource: any; // TODO: TEMP remove me
  editionStructure: any; // TODO: TEMP remove me

  constructor(
    public domUtils: DomUtilsService,
    private http: HttpClient,
    private configuration: ConfigurationService,
    private structureXML: StructureXmlParserService
  ) {
  }

  ngOnInit() {
    this.loadEditionStructure();
  }

  private loadEditionStructure(): void {
    this.configuration.getFileConfig()
      .subscribe((fileConfig: FileConfig) => {
        console.log(fileConfig);
        this.fileConfig = fileConfig;
        this.loadEdition()
          .subscribe((source) => {
            this.structureXML.getStructure(source)
              .subscribe(structure => this.editionStructure = structure);
          });
      });
  }

  private loadEdition() {
    if (this.editionSource) {
      return of(this.editionSource);
    } else {
      // const headers = new HttpHeaders({ 'Content-Type': 'text/xml' }).set('Accept', 'text/xml')
      return this.http.get(this.fileConfig.editionUrls[0], { responseType: 'text' })
        .pipe(tap((source) => this.editionSource = source));
    }
  }
}
