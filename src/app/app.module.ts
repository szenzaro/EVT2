import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { L10nConfig, L10nLoader, TranslationModule, StorageStrategy, ProviderType } from 'angular-l10n';

import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';

import { MainHeaderComponent } from './components/generic-ui/main-header/main-header.component';

import { ConfigurationService } from './configuration/configuration.service';
import { XmlUtilsService } from './services/xml-parsers/xml-utils.service';
import { StructureXmlParserService } from './services/xml-parsers/structure-xml-parser.service';
import { DomUtilsService } from './services/dom-utils.service';
import { GenericParserService } from './services/xml-parsers/generic-parser.service';

import { DocumentComponent } from './components/document/document.component';
import { PageComponent } from './components/page/page.component';
import { TextComponent } from './components/text/text.component';
import { GenericElementComponent } from './components/generic-element/generic-element.component';
import { ContentViewerComponent } from './components/content-viewer/content-viewer.component';
import { NoteComponent } from './components/note/note.component';
import { NamedEntityComponent } from './components/named-entity/named-entity/named-entity.component';
import { NamedEntityRefComponent } from './components/named-entity/named-entity-ref/named-entity-ref.component';
import { JsUtilsService } from './services/js-utils.service';

const l10nConfig: L10nConfig = {
  locale: {
    languages: [
      { code: 'en', dir: 'ltr' },
      { code: 'it', dir: 'ltr' }
    ],
    language: 'en',
    storage: StorageStrategy.Cookie
  },
  translation: {
    providers: [
      { type: ProviderType.Static, prefix: './assets/i10n/locale-' }
    ],
    caching: true,
    missingValue: 'No key'
  }
};

@NgModule({
  declarations: [
    AppComponent,
    MainHeaderComponent,
    DocumentComponent,
    PageComponent,
    TextComponent,
    GenericElementComponent,
    ContentViewerComponent,
    NoteComponent,
    NamedEntityComponent,
    NamedEntityRefComponent
  ],
  imports: [
    AngularFontAwesomeModule,
    BrowserModule,
    HttpClientModule,
    HttpModule,
    TranslationModule.forRoot(l10nConfig)
  ],
  providers: [
    ConfigurationService,
    XmlUtilsService,
    StructureXmlParserService,
    DomUtilsService,
    GenericParserService,
    JsUtilsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public l10nLoader: L10nLoader) {
    this.l10nLoader.load();
  }
}
