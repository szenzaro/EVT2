import { Injectable } from '@angular/core';

import { DomUtilsService } from '../dom-utils.service';

@Injectable()
export class GenericParserService {

  constructor(private DOMutils: DomUtilsService) { }

  parseText(xml: HTMLElement): Promise<TextData> {
    const text = { type: 'text', text: xml.textContent } as TextData;
    return Promise.resolve(text);
  }

  parseElement(xml: HTMLElement): Promise<GenericElementData> {
    const genericElement: GenericElementData = {
      type: 'generic-element',
      class: xml.tagName,
      content: xml.childNodes
    };
    return Promise.resolve(genericElement);
  }

  parseNote(xml: HTMLElement): Promise<NoteData> {
    const noteElement: NoteData = {
      type: 'note',
      xpath: this.DOMutils.xpath(xml),
      content: xml.childNodes
    };
    return Promise.resolve(noteElement);
  }

  parseEntityRef(xml: HTMLElement): Promise<NamedEntityRef> {
    const xpath = this.DOMutils.xpath(xml);
    const ref = xml.getAttribute('ref');
    const namedEntityElement = {
      type: 'named-entity',
      xpath: xpath,
      entityType: xml.tagName,
      content: xml.childNodes,
      entityId: ref ? ref.replace(/#/g, '') : xpath
    } as NamedEntityRef;
    return Promise.resolve(namedEntityElement);
  }

  parse(xml: HTMLElement): Promise<any> {
    if (xml) {
      if (xml.nodeType === 3) {  // Text
        return this.parseText(xml);
      }
      if (xml.nodeType === 8) { // Comment
        Promise.resolve({ type: 'comment' });
      }

      switch (xml.tagName) {
        // case 'rdg':
        // 	return this.parseRdg(xml);
        // case 'body':
        // 	return this.parseBody(xml)
        case 'note':
          return this.parseNote(xml);
        case 'placeName':
        case 'persName':
        case 'orgName':
          return this.parseEntityRef(xml);
        default:
          return this.parseElement(xml);
      }
    } else {
      return Promise.resolve();
    }
  }
}

export interface GenericElementData {
  type: string;
  class: string;
  content: any;
}

export interface TextData {
  type: 'text';
  text: string;
}

export interface NoteData {
  type: 'note';
  xpath: string;
  content: any;
}

export interface NamedEntityRef {
  type: 'named-entity';
  xpath: string;
  entityType: string;
  content: any;
  entityId: string;
}
