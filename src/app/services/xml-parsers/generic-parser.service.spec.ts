import { TestBed, inject } from '@angular/core/testing';

import { GenericParserService } from './generic-parser.service';

describe('GenericParserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GenericParserService]
    });
  });

  it('should be created', inject([GenericParserService], (service: GenericParserService) => {
    expect(service).toBeTruthy();
  }));
});
