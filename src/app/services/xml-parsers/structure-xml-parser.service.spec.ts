import { TestBed, inject } from '@angular/core/testing';

import { StructureXmlParserService } from './structure-xml-parser.service';

describe('StructureXmlParserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StructureXmlParserService]
    });
  });

  it('should be created', inject([StructureXmlParserService], (service: StructureXmlParserService) => {
    expect(service).toBeTruthy();
  }));
});
