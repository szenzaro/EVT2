import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { PageData } from '../../edition-data/page';

import { XmlUtilsService } from './xml-utils.service';
import { DomUtilsService } from '../dom-utils.service';

@Injectable()
export class StructureXmlParserService {

  constructor(
    private xmlUtils: XmlUtilsService,
    private domUtils: DomUtilsService) { }

  getStructure(document: any): Observable<any> {
    const pages: PageData[] = [];
    const pageTagName = 'pb';
    if (document) {
      if (typeof document !== 'object' && typeof document === 'string') {
        document = this.xmlUtils.parseXml(document);
      }
      // console.time('getStructure');
      const pageElements = document.querySelectorAll(pageTagName);
      console.log(pageElements);
      const l = pageElements.length;
      for (let i = 0; i < l; i++) {
        const element = pageElements[i];
        let pageContent: any[] = [];
        if (i < l - 1) { // TODO: handle last page
          pageContent = this.domUtils.getElementsBetweenTree(element, pageElements[i + 1]);
        }
        const page: PageData = {
          id: element.getAttribute('xml:id') || 'page_' + (pages.length + 1),
          label: element.getAttribute('n') || 'Page ' + (pages.length + 1),
          xmlSource: element,
          content: pageContent
        };
        pages.push(page);
      }
      // console.timeEnd('getStructure');
      console.log(pages);
    }
    return of({
      pages: pages
    });
  }

}
