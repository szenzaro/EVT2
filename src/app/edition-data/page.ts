export interface PageData {
  id: string;
  label: string;
  xmlSource: any;
  content: any[];
}
