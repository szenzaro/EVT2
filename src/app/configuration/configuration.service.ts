import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import { EVTConfig, UiConfig, EditionConfig, FileConfig } from './evt-config';

@Injectable()
export class ConfigurationService {
  private config: EVTConfig = {
    uiConfig: undefined,
    editionConfig: undefined,
    fileConfig: undefined
  };

  private uiConfigUrl = 'assets/config/ui_config.json';
  private fileConfigUrl = 'assets/config/file_config.json';
  private editionConfigUrl = 'assets/config/edition_config.json';

  constructor(private http: HttpClient) { }

  getUiConfig(): Observable<UiConfig> {
    if (this.config.uiConfig) {
      return of(this.config.uiConfig);
    } else {
      return this.http.get<any>(this.uiConfigUrl)
        .pipe(
          tap(uiConfig => this.config.uiConfig = uiConfig),
          catchError(this.handleError('getUiConfig', {}))
        );
    }
  }

  getFileConfig(): Observable<FileConfig> {
    if (this.config.fileConfig) {
      return of(this.config.fileConfig);
    } else {
      return this.http.get<any>(this.fileConfigUrl)
        .pipe(
          tap(fileConfig => this.config.fileConfig = fileConfig),
          catchError(this.handleError('getFileConfig', {}))
        );
    }
  }

  getEditionConfig(): Observable<EditionConfig> {
    if (this.config.editionConfig) {
      return of(this.config.editionConfig);
    } else {
      return this.http.get<any>(this.editionConfigUrl)
        .pipe(
          tap(editionConfig => this.config.editionConfig = editionConfig),
          catchError(this.handleError('getEditionConfig', {}))
        );
    }
  }

  /**
	* Handle Http operation that failed.
	* Let the app continue.
	* @param operation - name of the operation that failed
	* @param result - optional value to return as the observable result
	*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
